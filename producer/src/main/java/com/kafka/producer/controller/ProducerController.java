package com.kafka.producer.controller;

import com.kafka.producer.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("kafka")
public class ProducerController {

    @Autowired
    KafkaTemplate<String,Student> kafkaTemplate;

    public String topic="Student-topic";

    @PostMapping("/push")
    public String push(@RequestBody Student student){
        kafkaTemplate.send(topic,student);
        return "Published Successfully";
    }

}
