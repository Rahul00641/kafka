package com.example.demo;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {

    @KafkaListener(topics = "Student-topic", groupId = "group",containerFactory = "studentConcurrentKafkaListenerContainerFactory")
    public void pull(Student student){
        System.out.println("Fetched");
    }

}
